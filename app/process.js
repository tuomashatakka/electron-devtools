'use babel';
var proc = require('child_process')
var path = require('path')
var parent = module.parent
var rundir = path.resolve(parent ? (parent.filename, '..') : __dirname)




class RuntimeManager {

  constructor(args={}) {
    const electron = require('electron')
    let dir = args.dir || path.join(rundir, 'application.js')

    this.window = null
    this.isVisible = false
    this.process = proc.spawn(electron, [dir])
    /*this.process.on([
      'ready', this.getWindow(),
      'window-all-closed', () => this.process.platform !== 'darwin' ? this.process.quit() : false,
      'activate', () => this.window = !this.window ? this.getWindow() : false,
    ])*/
  }
/*
  getWindow(attrs={width: 640, height: 480, frame: false}) {
    if(this.window)
      return this.window
    return this.window
    this.window = new BrowserWindow(attrs)
    this.window.on('closed', () => this.window = null)
    return this.window
  }

  loadContent(url='./') {
    let win = this.getWindow()
    console.log(win);
    win.loadURL(url)
  }

  toggle(visibility=null) {
    if (visibility === null)
      return !this.isVisible
    console.log("VISIBLE")
    return visibility
  }

  on(evt, fnc) {
    proc.on(evt, fnc)
  }
*/
}


const runtime = new RuntimeManager()



module.exports = runtime
