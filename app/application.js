'use babel';
var path = require('path')
var parent = module.parent
var rundir = path.resolve(__dirname)
console.log(parent, rundir)

console.log(module, module.parent)


class RuntimeManager {

  constructor(args={}) {
    const electron = require('electron')
    this.application = electron.app

    this.window = null
    this.windowType = electron.BrowserWindow
    this.isVisible = false
    this.register({events:{
      'ready': () => this.loadContent('index.html'),
      'window-all-closed': () => this.application.platform !== 'darwin' ? this.application.quit() : false,
      'activate': () => this.loadContent('index.html'),
    }})
  }

  getWindow(attrs={width: 640, height: 480, frame: false}) {
    if(this.window)
      return this.window


    this.window = new this.windowType(attrs)
    this.window.on('closed', () => this.window = null)
    return this.window
  }

  loadContent(url='./') {
    let win = this.getWindow()
    console.log(win);
    let dir = path.join(rundir, url)
    win.loadURL('file://' + dir)
  }

  toggle(visibility=null) {
    if (visibility === null)
      return !this.isVisible
    console.log("VISIBLE")
    return visibility
  }

  register({context='application', events, single=null}) {
    if(single)
      events = {[events]: single}

    for(let evt in events) {
      this[context].on(evt, () => events[evt]())
    }
  }

}


const runtime = new RuntimeManager()



module.exports = runtime
