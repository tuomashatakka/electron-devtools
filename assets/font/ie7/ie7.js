/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'duality\'">' + entity + '</span>' + html;
	}
	var icons = {
		'dl-home-outline': '&#x68;',
		'dl-home': '&#x48;',
		'dl-mail-outline': '&#x65;',
		'dl-mail': '&#x45;',
		'dl-message-outline': '&#x6d;',
		'dl-message': '&#x4d;',
		'dl-phone-alt-outline': '&#x5b;',
		'dl-phone-alt': '&#x5d;',
		'dl-phone-outline': '&#x28;',
		'dl-phone': '&#x29;',
		'dl-picture-outline': '&#x70;',
		'dl-picture': '&#x50;',
		'dl-save': '&#x53;',
		'dl-save-outline': '&#x73;',
		'dl-display-outline': '&#x64;',
		'dl-display': '&#x44;',
		'dl-globe-outline': '&#x67;',
		'dl-globe': '&#x47;',
		'dl-camera-outline': '&#x63;',
		'dl-camera': '&#x43;',
		'dl-location-outline': '&#x6c;',
		'dl-location': '&#x4c;',
		'dl-book-outline': '&#x62;',
		'dl-book': '&#x42;',
		'dl-dots-outline': '&#x2e;',
		'dl-user-outline': '&#x75;',
		'dl-user': '&#x55;',
		'dl-dots': '&#x2026;',
		'dl-menu': '&#x2d;',
		'dl-chevron-down': '&#x3f;',
		'dl-chevron-left': '&#x3c;',
		'dl-chevron-up': '&#x3d;',
		'dl-chevron-right': '&#x3e;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/dl-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
