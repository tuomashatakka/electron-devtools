{
  "context-menu": {
    "atom-text-editor": [
      {
        "label": "Toggle Extended Tree View",
        "command": "extended-tree-view:toggle"
      }
    ]
  },
  "menu": [
    {
      "label": "View",
      "submenu": [
        {
          "label": "Toggle",
          "command": "pinned-bookmarks:toggle"
        }
      ]
    }
  ]
}
