

export const notify = (message="no message", details=null, icon=null) => {
  atom.notifications.addInfo(message, {
    detail: details || null,
    dismissable: false,
    icon: icon || 'circle-slash'
  })
  return false
}
