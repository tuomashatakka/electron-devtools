const EventEmitter = require('events')

var root = null
const RuntimeRenderer = {

    constructor: (context='main') => {
        root = document.querySelector(context)
        return RuntimeRenderer
    },

    render: (content='') => {
        root.innerHTML = content
        return content
    }
}

module.exports = (args) => RuntimeRenderer.constructor(args)
