const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow

let mainWindow
let sidePanel
function createWindow () {
  mainWindow = new BrowserWindow({width: 800, height: 600})
  sidePanel = new BrowserWindow({width: 400, height: 600, frame: false})
  console.log(mainWindow)
  console.log(sidePanel)
  console.log(app)
  mainWindow.loadURL(`file://${__dirname}/index.html`)
  sidePanel.loadURL(`file://${__dirname}/index.html`)
  mainWindow.webContents.openDevTools()
  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin')
    app.quit()
})

app.on('activate', () => {
  if (mainWindow === null)
    createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
